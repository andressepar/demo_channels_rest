"""
El repo contiene dos ramas. en master no se 
implementa la autenticacion para las consultas
por rest. En la rama authenticate_users podremos 
encontrar la configuracion para ello y un modelo
que contiene propietario.
""" 

#############################
#### Channels Y Rest FUll ###
#############################

######   CHANNELS   #########


*Settings 

Para usar channels debemos incluir en el settings los siguiente:

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(redis_host, redis_port)],
        },
        "ROUTING": "demo.routing.channel_routing",
    },
}

En el campo routing especificamos el path a las rutas que usara channels. Es analogo al urls.py

* Routing : 

En el routing especificamos las urls con las que podremos conectar desde el cliente socket. 

channel_routing = [
    route_class(Demultiplexer_progressbar, path="^/demo/binding/"),
    # route_class(Demultiplexer_smartCS, path="^/smartCS/binding/"),
]

Se especificara el metodo consumer de una clase ubicada en el consumer.py, normalmente en cada aplicacion.
Dentro del json consumers se pueden especificar distintos atributos que corresponderan con cada  uno de los 
modelos bindeados. 

class Demultiplexer_progressbar(WebsocketDemultiplexer):

    consumers = {
        
        "progressbar": ProgressBarBinding.consumer,
        "progressbar_user": ProgressBarUserBinding.consumer,

    }

    groups = ["binding.values"]

La clase {Model}Binding con la que se setea cada atributo, es una clase analoga a los modelos, donde se podra 
especificar los campos de los modelos que se desea sincronizar con el cliente. 

El modelo django de ProgressBar seria:

class ProgressBar(models.Model):

	"""
	Demo Model for progress bar view
	"""

	quantity = models.FloatField(default = 0)
	minim    = models.FloatField(default = 0)
	maxim    = models.FloatField(default = 100)


Y el modelo Binding, que hereda de la clase WebsocketBinding, y que modela la sincronizacion seria: 


class ProgressBarBinding(WebsocketBinding):

    model = ProgressBar
    stream = "progressbar"
    fields = ["__all__"]


    @classmethod
    def group_names(cls, *args, **kwargs):
        return ["binding.values"]

    def has_permission(self, user, action, pk):
        return True


Una vez configurado el backend, en el navegador deberemos incluir la funcion para conectar a una ruta socket: 

En nuestro proyecto de prueba estara en progressbar.static.js.progress_bar.js

connectSockets()  {
        console.log("Updating websockets through "+this.ws_scheme + '://' + window.location.host + "/demo/binding/")
        this.ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        this.ws_path = this.ws_scheme + '://' + window.location.host + "/demo/binding/";
        var custom_location = "";
        this.socket = new ReconnectingWebSocket(this.ws_path);
        this.socket.onmessage = function(message){
            Progressbar.manage_actions(message);
        };
        // Helpful debugging
        this.socket.onopen = () => { console.log("Connected to notification socket"); }
        this.socket.onclose = () => { console.log("Disconnected to notification socket"); }
        // var ws_path = ws_scheme + '://' + window.location.host + window.location.pathname + "binding/"; 
    }

Con este metodo, se conectara. Las ultimas dos funciones son para facilitar el debug en caso de errores en la conexion. 

La funcion onmessage gestiona el evento cuando llega un nuevo mensage. El mensage sera como el siguiente: 


{"isTrusted":false,"data":"{\"payload\": {\"action\": \"update\", \"pk\": 3, \"model\": \"progressbar.progressbar\", \"data\": {\"minim\": 0.0, \"maxim\": 100.0, \"quantity\": 3}}, \"stream\": \"progressbar\"}"}




######   Django REST FULL   #########


Django Rest es una extension para django que nos proveera una api para
implementar de la forma mas rapida una api siguiendo el standard REST.

El standard REST especifica una serie de normas para hacer una api que, 
mas adelante, facilitaran el mantenimiento y la documentacion. 

El standard REST especifica una accion especifica para cada verbo contemplado
en el protocolo HTTP : 

PATCH : actualizar recurso
POST  : crear recurso
DELETE: borrar un registro concreto de un recurso
GET   : consultar recurso

Asi por ejemplo en nuestra api, para consultar el recurso progressbar con el 
cliente http escrito en python, que se utiliza desde la consola: 

http GET http://{domain}:{port}/api/progressbar/

Si nos fijamos en la parte de la url "/api/" veremos que es la url que hemos 
especificado en nuestro urls.py principal, donde designamos una url para la api. 

Si consultamos esta url, nos devolvera todos los recursos disponibles en nuestra api. 

Si quisieramos consultar el recurso progressbar_user, deberemos usar las credenciales, 
ya que este recurso tiene usuarios asignados, y lo hemos usado para ejemplificar el uso 
necesario de credenciales para ciertos recursos. 

http -a admin:sistemas GET http://{domain}:{port}/api/progressbar_user/

En el endpoint para este recurso, hemos implementado la logica por la cual se devolvera en 
esta consulta, unicamente, los registros del recurso que son propiedad del usuario que se autentica
en la consulta. Podriamos haber implementado otro tipo de logica con la que se devolvieran todos los
registros si el usuario que consulta el superuser por ejemplo. 

En el standard REST, los datos de autenticacion se deben incluir en cada consulta para evitar
que el servidor persista variables de sesion, que en el caso de aplicaciones con muchos usuarios puede
llegar a ser algo pesado en lo que a RAM respecta. 

Tambien se podria especificar un tipo de autenticacion por token, que resulta mas seguro en el caso de 
hacer consultas desde el propio navegador. Es una metodologia habitual el pasar a la desarrolladora 
los endpoints de la api y hacer la app REST, lo que permite cambiar el lenguaje de frontend o el framework 
mucho mas agilmente, ya que el backend y los modelos se pueden mantener estaticos. Por otro lado, 
tambien nos permite hacer aplicaciones complementarias para movil u otros dispositivos, sin tener que
tocar el core de la app. 

* Configuracion de la api

Para implementar la api, lo primero que debemos hacer es añadir la app rest_framework
entre las apps instaladas del settings de django. rest_framework es la extension que nos facilitara
todo el proceso. A continuacion setearemos la variable

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        #'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}

en el settings de django. Que especificara el tipo de autenticacion que queremos usar en la api. 

A continuacion, en el urls principal de la aplicacion, crearemos una nueva url en el urlpatterns 
que sera el endpoint principal de la api, para ello deberemos importar routers de las variables de :

from .api_routers import router

que esta en la misma carpeta. Personalmente prefiero separar todo lo posible el codigo. 

En demo.api_routers incluiremos los modelos que deseamos proveer en forma de recursos desde nuestra api: 

from rest_framework import routers
from progressbar.serializers import *


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'progressbar', ProgressBarViewSet)
router.register(r'progressbar_user', ProgressBarUserViewSet)


Como se ve, se a de importar la clase routers de la app rest_framework. Esta clase contiene el metodo
estatico DefaultRouter que iniciara las rutas para a continuacion añadir los recursos. Rescto a rutas, 
routers se encargara de crear los endpoints necesarios para cada recurso por lo que no tendremos que 
hacer nada mas. 

Cada recurso añadido tendra un ViewSet asociado, que es precisamente el endpoint para el 
recurso en particular. Dentro de este se haran las consultas necesarias para retornar los datos del 
recurso, borrar o actualizar. 

Como se puede ver en la parte de arriba, los ViewSet se alojan en el archivo de serializers de la app 
progressbar, ya que los modelos a los que apunta son los de esta app. No esta demas insistir en que 
es importante que todo este bien colocado, por eso los ViewSet se ponen aparte junto con los serializadores, 
y dentro de la app a la que apunta el recurso que provee. 

Como deciamos el ViewSet implementa el endpoint del recurso, pero es necesario serializar la consulta del 
modelo para poder enviarla por socket como un string. Para ello usaremos las clases serializadoras. 

class ProgressBarViewSet(viewsets.ModelViewSet):
    
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = ProgressBar.objects.all()
    serializer_class = ProgressBarSerializer



class ProgressBarUserSerializer(serializers.ModelSerializer):

    class Meta:

        model = ProgressBarUser
        fields = ('id', 'owner', 'quantity', 'minim', 'maxim',)


La clase ViewSet hereda de viewsets.ModeViewSet y la clase serializadora, de serializers.ModelSerializer. 
En esta ultima se puede especificar el formato en que queremos que se retorne la consulta, y tambien
podremos serializar campos que apunte a otros modelos como many to many o foreign key. 

EN la clase viewset podremos especificar la consulta para implementar la logica entre la consulta y el 
retorno, o los permisos. 

Aasi por ejemplo en el ViewSet de ProgressBarUser, que contiene un propietario owner:

class ProgressBarUserSerializer(serializers.ModelSerializer):

    class Meta:

        model = ProgressBarUser
        fields = ('id', 'owner', 'quantity', 'minim', 'maxim',)


class ProgressBarUserViewSet(viewsets.ModelViewSet):
    
    """
    API endpoint that allows users to be viewed or edited.
    """
    #queryset = ProgressBarUser.objects.all()
    serializer_class = ProgressBarUserSerializer

    # Without this, the request is from anonymous user
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        return ProgressBarUser.objects.filter(owner=user.pk)

    def get(self, request, format=None):
        content = {
            'user': unicode(request.user),  # `django.contrib.auth.User` instance.
            'auth': unicode(request.auth),  # None
        }
        return Response(content)


En el metodo get_queryset especificaremos la consulta cogiendo los datos del usuario que 
hace la autenticacion en la propia consulta. 


